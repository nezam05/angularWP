(function () {
	var myApp = angular.module('myApp', ['ui.router','ui.bootstrap']);
	myApp.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('app', {
				url: '/',
				templateUrl: 'index2.html',
				controller: 'homeCtrl'
			})
			.state('detail', {
				url: '/:slug',
				templateUrl: 'home.html',
				controller: 'detailCtrl',
				params: {
					singlePost: null
				}
			});
		$urlRouterProvider.otherwise('/');
	});
	myApp.config( function( $httpProvider ) {
		$httpProvider.interceptors.push( [ function() {
		  return {
			'request': function( config ) {
				console.log("inside interceptor");
			  config.headers = config.headers || {};
			  config.headers.Authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvbWF0cml0dG8uY29tIiwiaWF0IjoxNTQzOTAzODIzLCJuYmYiOjE1NDM5MDM4MjMsImV4cCI6MTU0NDUwODYyMywiZGF0YSI6eyJ1c2VyIjp7ImlkIjoiNjcifX19.ciHbx7SubEE8Ya4KxN-Vi-cRZ3KN-S7z_4n0TyCGKxI';
			  console.log("manual"+config.headers);
			  return config;
			}
		  };
		}]);
	  } );
	myApp.controller("homeCtrl", function ($scope, $http, $sce) {
		
		$scope.filteredTodos = [];
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 5;
		$scope.apiLoaded = 0;
		$scope.doSearch='';
		$scope.toggleAddPost=0;

		var onSuccess = function (data, status, headers) {
			$scope.apiLoaded = 1;
			$scope.posts = data;
			console.log("headers:",headers);
			let header = headers();
			$scope.totalElements = header['x-wp-total'];
			console.log("here ",$scope.totalElements, header);
			for (let i = 0; i < $scope.posts.length; i++) {
				console.log("here");
				$scope.posts[i].excerpt.rendered = $sce.trustAsHtml($scope.posts[i].excerpt.rendered);
			}
		};

		
		var onError = function (data, status) {
			$scope.error = status;
		};
		
		$scope.$watch('currentPage',function(){
			console.log("page changed");
			$scope.apiLoaded = 0;
			var promise = $http.get("https://matritto.com/wp-json/wp/v2/posts?page="+$scope.currentPage, { cache: true });
			promise.success(onSuccess);
			promise.error(onError);

		});

		$scope.doSearch = function(){
			console.log($scope.doSearch);
			$scope.apiLoaded = 0;
			var promise = $http.get("https://matritto.com/wp-json/wp/v2/posts?search="+$scope.searchTerm, { cache: true });
			promise.success(onSuccess);
			promise.error(onError);

		};

		$scope.addNewPost=function(){
			$scope.toggleAddPost = 0;
			postData={"title":$scope.postTitle,
						"content":$scope.postBody,
						"status":"draft"	
				}
			console.log("New post will be added as draft.");
			$scope.apiLoaded = 0;
			var promise = $http({
				url:"https://matritto.com/wp-json/wp/v2/posts/",
				data: postData,
				method:"POST",
				headers:{"Content-type":"application/json"}
			});
			promise.success(getAllPost);
			promise.error(onError);
			
		};
		var getAllPost=function(){
			console.log("inside getallpost");
			var promise = $http.get("https://matritto.com/wp-json/wp/v2/posts/", { cache: true });
			promise.success(onSuccess);
			promise.error(onError);
			clearForm();
		};
		var clearForm=function(){
			console.log("clearing");
			$scope.postTitle='';
			$scope.postBody='';
			
		}
	});
	myApp.controller("detailCtrl", function ($scope, $http, $sce, $stateParams) {
		console.log("params::", $stateParams);
		$scope.post = $stateParams.singlePost;
		$scope.post.content.rendered = $sce.trustAsHtml($scope.post.content.rendered);
	});

})()
